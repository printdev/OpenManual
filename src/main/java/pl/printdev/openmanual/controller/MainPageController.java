package pl.printdev.openmanual.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainPageController {

  @GetMapping("index")
  public ResponseEntity<String> getIndex() {
    return new ResponseEntity<>("Hello World", HttpStatus.OK);
  }

}
