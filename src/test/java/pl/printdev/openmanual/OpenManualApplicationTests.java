package pl.printdev.openmanual;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.printdev.openmanual.controller.MainPageController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenManualApplicationTests {

  @Autowired
  private MainPageController mainPageController;

  @Test
  public void contextLoads() {
    assertThat(mainPageController).isNotNull();
  }

}
